
/*
 * Copyright 2018 Geno Papashvili
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ge.gnio;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class Packet {

    private int key;
    private ByteBuffer buffer;

    public Packet(int key, ByteBuffer buffer) {
        if(buffer == null){
            this.buffer = ByteBuffer.allocate(1);
            this.buffer.order(ByteOrder.LITTLE_ENDIAN);
            byte b = -1;
            this.buffer.put(b);
            this.buffer.flip();
        }else{
            this.buffer = buffer;
        }
        this.key = key;
    }

    public Packet(int key) {
        this(key,null);
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public ByteBuffer getBuffer() {
        return buffer;
    }

    public void setBuffer(ByteBuffer buffer) {
        this.buffer = buffer;
    }
}