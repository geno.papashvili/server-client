
/*
 * Copyright 2018 Geno Papashvili
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ge.gnio;

import com.google.common.base.Preconditions;

import java.io.IOException;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.ReentrantLock;

public class Client {

    private SelectionKey selectionKey;
    private SocketChannel channel;
    private LinkedBlockingQueue<Packet> sendPacketQueue;

    private SecondaryPacketListener secondaryPacketListener = null;

    private boolean connected = false;

    private final Server _server;

    private ReentrantLock reentLocker;

    private Object  attached = null;


    Client(Server server, SelectionKey selectionKey) {
        this._server = server;
        this.channel = (SocketChannel) selectionKey.channel();
        this.selectionKey = selectionKey;
        this.sendPacketQueue = new LinkedBlockingQueue<>();
        this.reentLocker = new ReentrantLock();

        try {
            connected = channel.finishConnect();
        } catch (Exception ignored) {

        }
    }

    SelectionKey getSelectionKey() {
        return selectionKey;
    }

    public SocketChannel getChannel() {
        return channel;
    }

    public void close() {
        try {
            channel.close();
        } catch (IOException e) {
            // e.printStackTrace();
        }
        selectionKey.cancel();
        connected = false;
    }

    public Server getServer() {
        return _server;
    }

    public boolean isConnected() {
        return connected;
    }

    public synchronized void sendPacket(Packet packet) {
        sendPacketQueue.add(packet);
        selectionKey.interestOps(SelectionKey.OP_WRITE);
    }

    public <T> T sendPacket(Packet packet, PacketListener<T> packetListener, long timeout) {

        Preconditions.checkState(!Objects.equals(Thread.currentThread(),getServer().getSelectorThread()),"try another thread");
        Preconditions.checkState(_server.getPacketListener(packet.getKey()) == null,"Key already exists");

        try {
            reentLocker.lock();
            final CountDownLatch latch = new CountDownLatch(1);
            final AtomicReference<T> value = new AtomicReference<>();

            secondaryPacketListener = new SecondaryPacketListener(packet.getKey()) {
                @Override
                public void readPacket(Client client, Packet packet) {
                    value.set(packetListener.readPacket(client, packet));
                    latch.countDown();
                }
            };

            sendPacket(packet);

            try {
                latch.await(timeout, TimeUnit.MILLISECONDS);
            } catch (InterruptedException ignored) {

            }
            secondaryPacketListener = null;
            return value.get();
        } finally {
            reentLocker.unlock();
        }
    }

    SecondaryPacketListener getSecondaryPacketListener() {//package private
        return secondaryPacketListener;
    }

    static abstract class SecondaryPacketListener {

        public final int KEY;

        public abstract void readPacket(Client client, Packet packet);

        SecondaryPacketListener(int key) {
            this.KEY = key;
        }

    }

    @SuppressWarnings("unchecked")
    public <T> T attached(){
        return  (T) attached;
    }

    public void attach(Object object){
        this.attached = object;
    }


    LinkedBlockingQueue<Packet> getSendPacketQueue() {
        return sendPacketQueue;
    }
}
