/*
 * Copyright 2018 Geno Papashvili
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ge.gnio;

import com.google.common.base.MoreObjects;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.ImmutableList;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.*;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.locks.LockSupport;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Server {


    private static Logger logger = Logger.getLogger(Server.class.getSimpleName());

    private Selector selector;
    private ServerSocketChannel serverSocketChannel;

    private InetSocketAddress inetSocketAddress;


    private Thread selectorThread;

    private final ExecutorService executorService;

    private boolean eof = false;

    private final ByteBuffer SIZE_BUFFER = ByteBuffer.allocate(Short.BYTES).order(ByteOrder.LITTLE_ENDIAN);

    private InetAddressFilter inetAddressFilter;

    private static Integer countServer = 0;

    private String name;

    private BiMap<PacketListener, Integer> packetsListeners = HashBiMap.create();


    public Server(String host, int port, InetAddressFilter inetAddressFilter, ExecutorService executorService) throws IOException {
        synchronized (this) {
            name = getClass().getSimpleName() + "-" + (++countServer);
        }

        if (host == null) {
            inetSocketAddress = new InetSocketAddress(port);
        } else {
            inetSocketAddress = new InetSocketAddress(host, port);
        }

        this.inetAddressFilter = inetAddressFilter;

        this.executorService = executorService;


        selector = Selector.open();

    }


    public void open() throws IOException {
        if (eof || selectorThread != null) {
            throw new IllegalStateException("Server is open");
        }
        serverSocketChannel = ServerSocketChannel.open();
        serverSocketChannel.bind(inetSocketAddress);
        serverSocketChannel.configureBlocking(false);
        serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);

        selectorThread = new Thread(this::run);
        selectorThread.setName(name);
        selectorThread.start();

    }


    private void run() {

        Client client;
        Iterator<SelectionKey> selectedKeys;

        int count;

        eof = true;
        while (eof) {
            try {
                count = selector.selectNow();
            } catch (IOException e) {
                count = 0;
            }

            if (count > 0) {
                selectedKeys = selector.selectedKeys().iterator();
                while (selectedKeys.hasNext()) {
                    SelectionKey key = selectedKeys.next();
                    client = (Client) key.attachment();
                    switch (key.readyOps()) {
                        case SelectionKey.OP_ACCEPT:
                            accept();
                            break;
                        case SelectionKey.OP_CONNECT:
                            connect(client);
                            break;
                        case SelectionKey.OP_READ:
                            SIZE_BUFFER.clear();
                            reader(SIZE_BUFFER, client);
                            break;
                        case SelectionKey.OP_WRITE:
                            writer(client);
                            break;
                    }
                    selectedKeys.remove();
                }
            }

            LockSupport.parkNanos(100);
        }

        clear();
        try {
            serverSocketChannel.close();
        } catch (IOException e) {
            //e.printStackTrace();
        }

        selectorThread = null;
    }

    private void writer(Client client) {
        if (!client.getSendPacketQueue().isEmpty()) {
            Packet packet = client.getSendPacketQueue().poll();

            if (packet.getKey() > Short.MAX_VALUE || packet.getKey() < Short.MIN_VALUE) {
                throw new NumberFormatException("Packet key Min value is " + Short.MIN_VALUE + " and Max value is " + Short.MAX_VALUE);
            }

            ByteBuffer buffer = ByteBuffer.allocate(packet.getBuffer().remaining() + Short.BYTES * 2).order(ByteOrder.LITTLE_ENDIAN);

            buffer.putShort((short) (packet.getBuffer().remaining() + Short.BYTES));
            buffer.putShort((short) packet.getKey());
            buffer.put(packet.getBuffer());
            buffer.flip();

            int result = -1;
            try {
                result = client.getChannel().write(buffer);
            } catch (IOException e) {
                //ignore
            }
            if (result > 0) {
                writer(client);
            } else {
                client.close();
            }
        }

        try {
            client.getSelectionKey().interestOps(SelectionKey.OP_READ);
        } catch (CancelledKeyException e) {
            //if client close
        }

    }


    private void reader(ByteBuffer buffer, Client client) {
        int result = -1;
        try {
            result = client.getChannel().read(buffer);
            buffer.flip();
        } catch (IOException e) {
            //ignore
        }

        if (result >= Short.BYTES) {

            if (buffer.limit() == Short.BYTES) { // 2 byte read
                int dataSize = buffer.getShort();
                if (dataSize > 0) {
                    //recall
                    reader(ByteBuffer.allocate(dataSize).order(ByteOrder.LITTLE_ENDIAN), client);
                }
            } else if (buffer.limit() > Short.BYTES) {// packet read

                Runnable runnable = () -> {
                    int key = buffer.getShort();
                    byte[] bytes = new byte[buffer.remaining()];
                    buffer.get(bytes);

                    Packet packet = new Packet(key, ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN));

                    try {
                        packetsListeners.inverse().get(key).readPacket(client, packet);
                    } catch (Exception e) {
                        if (client.getSecondaryPacketListener() != null && client.getSecondaryPacketListener().KEY == key) {
                            client.getSecondaryPacketListener().readPacket(client, packet);
                        }
                    }
                };

                if (executorService != null) {
                    executorService.execute(runnable);
                } else {
                    runnable.run();
                }

            }
        } else if (result == -1) {
            client.close();
        }
    }

    private void connect(Client client) {
        logger.log(Level.INFO, "//connect");
    }


    private void accept() {
        SocketChannel socketChannel;
        try {
            while ((socketChannel = serverSocketChannel.accept()) != null) {
                if (inetAddressFilter != null && !inetAddressFilter.doAccept(socketChannel.socket().getInetAddress())) {
                    socketChannel.close();
                    return;
                }
                socketChannel.configureBlocking(false);
                SelectionKey selectionKey = socketChannel.register(selector, SelectionKey.OP_READ);
                Client client = new Client(this, selectionKey);
                selectionKey.attach(client);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public InetSocketAddress getInetSocketAddress() {
        return inetSocketAddress;
    }

    public void clear() {
       selector.keys().stream()
               .filter(k -> k.attachment() != null && k.attachment() instanceof Client)
               .map(k -> (Client) k.attachment())
               .forEach(c -> c.close());
    }


    public void addPacketListener(int key, PacketListener listener) {
        packetsListeners.put(listener, key);
    }

    public void removePacketListener(int key) {
        packetsListeners.inverse().remove(key);
    }

    public PacketListener getPacketListener(int key) {
        return packetsListeners.inverse().get(key);
    }

    public ImmutableList<Client> getClients(){
        return ImmutableList.copyOf(selector.keys().stream()
                .filter(k -> k.attachment() != null && k.attachment() instanceof Client)
                .map(k -> (Client) k.attachment())
                .iterator());
    }

    public void close() {
        if (!eof) {
            throw new IllegalStateException("Server not open");
        }
        eof = false;
    }


    public void shutdown() {
        if (eof) {
            close();
        }
        try {
            selector.close();
        } catch (IOException e) {
            //
        }
    }
    public boolean isClosed(){
        return (!eof && selectorThread == null);
    }

    Thread getSelectorThread() {
        return selectorThread;
    }

    public int countPacketListener(){
        return packetsListeners.size();
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(name)
                .add("Host",inetSocketAddress.getHostName())
                .add("Port",inetSocketAddress.getPort())
                .add("Opened",!isClosed())
                .add("ExecutorService",executorService != null)
                .add("ClientCount",getClients().size()).toString();

    }
}
