# Server-client
java Server and client


**Demo Server:**

```java
 package ge.demo;
 
 import ge.demo.listeners.CountClientListener;
 import ge.demo.listeners.HelloClientsListener;
 import ge.demo.listeners.HelloListener;
 import ge.gnio.Server;
 
 import java.io.IOException;
 import java.util.concurrent.ExecutorService;
 import java.util.concurrent.Executors;
 
 public class DemoServer {
 
     public static void main(String args[]) throws IOException {
 
         IpFilter filter = new IpFilter();
 
         ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
 
         Server server = new Server("localhost",9090,filter,executor);
 
         server.addPacketListener(10,new HelloListener());
         server.addPacketListener(20, new CountClientListener());
         server.addPacketListener(30,new HelloClientsListener());
 
         server.open();
         System.out.println("server start");
         System.out.println(server.getInetSocketAddress().getHostName());
     }
 }

```

Ip Filter:

```java
package ge.demo;

import ge.gnio.InetAddressFilter;

import java.io.BufferedReader;
import java.io.StringReader;
import java.net.InetAddress;


public class IpFilter implements InetAddressFilter {


    String blackList =
            //"127.0.0.1\n"+
            "10.10.10.2\n";
    @Override
    public boolean doAccept(InetAddress address) {
        try {
            BufferedReader reader = new BufferedReader(new StringReader(blackList));
            String line;
            while ((line = reader.readLine()) != null) {
                if(line.equals(address.getHostAddress())){
                    return false;
                }
            }
        }catch (Exception e){

        }
        return true;
    }
}
```
        
Packet Listener:

```java
package ge.demo.listeners;

import ge.gnio.Client;
import ge.gnio.Packet;
import ge.gnio.PacketListener;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class HelloListener implements PacketListener<Void> {

    @Override
    public Void readPacket(Client client, Packet packet) {

        long time = packet.getBuffer().getLong();

        Date date = new Date(time);
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        System.out.println(dateFormat.format(date));


        StringBuilder builder = new StringBuilder();
        while (true) {
            builder.append(packet.getBuffer().getChar());
            if(packet.getBuffer().position() == packet.getBuffer().limit()){
                break;
            }
        }
        System.out.println(builder.toString());


        packet.setBuffer(ByteBuffer.allocate(1024).order(ByteOrder.LITTLE_ENDIAN));
        packet.getBuffer().putLong(Calendar.getInstance().getTimeInMillis());
        String hello = "Hello Java Server";
        for (int i = 0; i < hello.length(); i++) {
            packet.getBuffer().putChar(hello.charAt(i));
        }
        packet.getBuffer().flip();
        client.sendPacket(packet);
        return null;
    }
}
```
```java
package ge.demo.listeners;

import ge.gnio.Client;
import ge.gnio.Packet;
import ge.gnio.PacketListener;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.List;
import java.util.concurrent.locks.LockSupport;

public class HelloClientsListener implements PacketListener<Void> {

    @Override
    public Void readPacket(Client client, Packet packet) {

        List<Client> clients = client.getServer().getClients();

        for (Client c : clients){
            c.sendPacket(new Packet(packet.getKey(),ByteBuffer.wrap(packet.getBuffer().array()).order(ByteOrder.LITTLE_ENDIAN)));
        }

        return null;
    }
}
```
```java
package ge.demo.listeners;

import ge.gnio.Client;
import ge.gnio.Packet;
import ge.gnio.PacketListener;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class CountClientListener implements PacketListener<Void> {
    @Override
    public Void readPacket(Client client, Packet packet) {
        int count  = client.getServer().count();
        packet.setBuffer(ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN));
        packet.getBuffer().putInt(count);
        packet.getBuffer().flip();
        client.sendPacket(packet);
        return null;
    }
}
```

**Client Socket**

```java
package ge.demo;

import ge.demo.listeners.HelloClientListener;
import ge.demo.listeners.HelloListener;
import ge.gnio.Client;
import ge.gnio.Packet;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Calendar;
import java.util.Scanner;

public class DemoClient {

    public static void main(String args[]) throws IOException {
        Client client = new Client("localhost", 9090, null);
        client.addPacketListener(10, new HelloListener());
        client.addPacketListener(30,new HelloClientListener());

        client.connect();

        Scanner scanner = new Scanner(System.in);

        ByteBuffer buffer = ByteBuffer.allocate(1024).order(ByteOrder.LITTLE_ENDIAN);


        String line;

        String name;

        System.out.println("Enter you name:");
        name = scanner.nextLine();

        boolean eof = true;
        while (eof) {

            line = scanner.nextLine();


            switch (line) {
                case "q":
                    eof = false;
                    break;
                case "h":
                    buffer.clear();

                    buffer.putLong(Calendar.getInstance().getTimeInMillis());

                    String hello = client.toString() + " Say: Hello Java Server";

                    for (int i = 0; i < hello.length(); i++) {
                        buffer.putChar(hello.charAt(i));
                    }
                    buffer.flip();
                    Packet packet = new Packet(10, buffer);
                    client.sendPacket(packet);
                    break;
                case "c":
                    int count = client.sendPacket(new Packet(20), (c, p) -> p.getBuffer().getInt());
                    System.out.println(count);
                    break;
                case "a":
                    buffer.clear();
                    System.out.print("sya: ");
                    String txt = scanner.nextLine();
                    buffer.putLong(Calendar.getInstance().getTimeInMillis());
                    buffer.put((name+" say: "+txt).getBytes());
                    buffer.flip();
                    client.sendPacket(new Packet(30,buffer));
            }
        }
        client.close();
    }
}

```

```java
package ge.demo.listeners;

import ge.gnio.Client;
import ge.gnio.Packet;
import ge.gnio.PacketListener;

import java.text.SimpleDateFormat;
import java.util.Date;

public class HelloClientListener implements PacketListener<Void> {
    @Override
    public Void readPacket(Client client, Packet packet) {
        Date date = new Date(packet.getBuffer().getLong());
        SimpleDateFormat format = new SimpleDateFormat();

        System.out.println(format.format(date));
        byte[] b = new byte[packet.getBuffer().remaining()];

        packet.getBuffer().get(b);

        System.out.println(new String(b));

        return null;
    }
}

```

```java
package ge.demo.listeners;

import ge.gnio.Client;
import ge.gnio.Packet;
import ge.gnio.PacketListener;

import java.text.SimpleDateFormat;
import java.util.Date;

public class HelloListener implements PacketListener<Void> {
    @Override
    public Void readPacket(Client client, Packet packet) {

        Date date = new Date(packet.getBuffer().getLong());
        SimpleDateFormat format = new SimpleDateFormat();
        System.out.println(format.format(date));
        StringBuilder builder = new StringBuilder();

        while (true) {
            builder.append(packet.getBuffer().getChar());
            if(packet.getBuffer().position() == packet.getBuffer().limit()){
                break;
            }
        }

        System.out.println(builder.toString());
        return null;
    }
}

```
