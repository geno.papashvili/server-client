
/*
 * Copyright 2018 Geno Papashvili
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ge.gnio;

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.CancelledKeyException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.LockSupport;
import java.util.concurrent.locks.ReentrantLock;


public class Client {

    private static Integer countClient = 0;
    private final String name;

    private Thread selectorThread;

    private Selector selector;
    private SocketChannel socketChannel;
    private SelectionKey selectionKey;

    private InetSocketAddress inetSocketAddress;

    private volatile boolean eof = false;

    private boolean connected = false;

    private final ByteBuffer SIZE_BUFFER = ByteBuffer.allocate(Short.BYTES).order(ByteOrder.LITTLE_ENDIAN);

    private final LinkedBlockingQueue<Packet> sendPacketQueue = new LinkedBlockingQueue<>();

    private final BiMap<PacketListener, Integer> packetsListeners = HashBiMap.create();

    private final ExecutorService executorService;

    private Object attached = null;


    private final ReentrantLock reentLocker = new ReentrantLock();
    private SecondaryPacketListener secondaryPacketListener = null;


    public Client(String host, int port, ExecutorService executorService) throws IOException {
        synchronized (this) {
            name = getClass().getSimpleName() + "-" + (++countClient);
        }
        inetSocketAddress = new InetSocketAddress(host, port);
        selector = Selector.open();

        this.executorService = executorService;

    }

    public void connect() throws IOException {
        if (eof || selectorThread != null) {
            throw new IllegalStateException("Channel is connected");
        }


        socketChannel = SocketChannel.open();
        socketChannel.connect(inetSocketAddress);
        socketChannel.configureBlocking(false);
        selectionKey = socketChannel.register(selector, SelectionKey.OP_READ);

        connected = socketChannel.finishConnect();

        selectorThread = new Thread(this::run);
        selectorThread.setName(name);
        selectorThread.start();

    }


    private void run() {
        int count;
        eof = true;
        while (eof) {

            try {
                count = selector.selectNow();
            } catch (IOException e) {
                count = 0;
            }

            if (count > 0) {
                Iterator<SelectionKey> selectionKeys = selector.selectedKeys().iterator();
                while (selectionKeys.hasNext()) {
                    SelectionKey key = selectionKeys.next();
                    switch (key.readyOps()) {
                        case SelectionKey.OP_CONNECT:
                            connection();
                            break;
                        case SelectionKey.OP_READ:
                            SIZE_BUFFER.clear();
                            reader(SIZE_BUFFER);
                            break;
                        case SelectionKey.OP_WRITE:
                            writer();
                            break;
                    }
                    selectionKeys.remove();
                }
            }

            LockSupport.parkNanos(100);
        }

        selectionKey.cancel();
        try {
            socketChannel.close();
        } catch (IOException e) {
            //
        }
        connected = false;
        selectorThread = null;
    }

    private void writer() {
        if (!sendPacketQueue.isEmpty()) {
            Packet packet = sendPacketQueue.poll();

            if (packet.getKey() > Short.MAX_VALUE || packet.getKey() < Short.MIN_VALUE) {
                throw new NumberFormatException("Packet key Min value is " + Short.MIN_VALUE + " and Max value is " + Short.MAX_VALUE);
            }

            ByteBuffer buffer = ByteBuffer.allocate(packet.getBuffer().remaining() + Short.BYTES * 2).order(ByteOrder.LITTLE_ENDIAN);

            buffer.putShort((short) (packet.getBuffer().remaining() + Short.BYTES));
            buffer.putShort((short) packet.getKey());
            buffer.put(packet.getBuffer());
            buffer.flip();

            int result = -1;
            try {
                result = socketChannel.write(buffer);
            } catch (IOException e) {
                //ignore
            }
            if (result > 0) {
                writer();
            } else {
                close();
            }
        }

        try {
            selectionKey.interestOps(SelectionKey.OP_READ);
        } catch (CancelledKeyException e) {
            //if client close
        }

    }


    private void reader(ByteBuffer buffer) {
        int result = -1;
        try {
            result = socketChannel.read(buffer);
            buffer.flip();
        } catch (IOException e) {
            //ignore
        }

        if (result >= Short.BYTES) {

            if (buffer.limit() == Short.BYTES) { // 2 byte read
                int dataSize = buffer.getShort();
                if (dataSize > 0) {
                    //recall
                    reader(ByteBuffer.allocate(dataSize).order(ByteOrder.LITTLE_ENDIAN));
                }
            } else if (buffer.limit() > Short.BYTES) {// packet read

                Runnable runnable = () -> {

                    int key = buffer.getShort();
                    byte[] bytes = new byte[buffer.remaining()];
                    buffer.get(bytes);

                    Packet packet = new Packet(key, ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN));

                    try {
                        packetsListeners.inverse().get(key).readPacket(this, packet);
                    } catch (Exception ignored) {
                        if (secondaryPacketListener != null && secondaryPacketListener.KEY == key) {
                            secondaryPacketListener.readPacket(this, packet);
                        }
                    }
                };

                if (executorService != null) {
                    executorService.execute(runnable);
                } else {
                    runnable.run();
                }

            }
        } else if (result == -1) {
            close();
        }
    }


    public synchronized void sendPacket(Packet packet) {
        sendPacketQueue.add(packet);
        selectionKey.interestOps(SelectionKey.OP_WRITE);
    }


    public <T> T sendPacket(Packet packet, PacketListener<T> packetListener, long timeout) {

        Preconditions.checkState(!Objects.equals(Thread.currentThread(), selectorThread), "try another thread");
        Preconditions.checkState(packetsListeners.inverse().get(packet.getKey()) == null, "Key already exists");

        try {
            reentLocker.lock();
            final CountDownLatch latch = new CountDownLatch(1);
            final AtomicReference<T> value = new AtomicReference<>();

            secondaryPacketListener = new SecondaryPacketListener(packet.getKey()) {
                @Override
                public void readPacket(Client client, Packet packet) {
                    value.set(packetListener.readPacket(client, packet));
                    latch.countDown();
                }
            };

            sendPacket(packet);

            try {
                latch.await(timeout, TimeUnit.MILLISECONDS);
            } catch (InterruptedException ignored) {

            }
            secondaryPacketListener = null;
            return value.get();
        } finally {
            reentLocker.unlock();
        }
    }

    private static abstract class SecondaryPacketListener {

        public final int KEY;

        public abstract void readPacket(Client client, Packet packet);

        SecondaryPacketListener(int key) {
            this.KEY = key;
        }

    }


    public void addPacketListener(int key, PacketListener listener) {
        packetsListeners.put(listener, key);
    }

    public void removePacketListener(int key) {
        packetsListeners.inverse().remove(key);
    }

    private void connection() {
        try {
            connected = socketChannel.finishConnect();
            selectionKey.interestOps(SelectionKey.OP_READ);
        } catch (IOException e) {
            close();
        }

    }

    public void close() {
        if (!eof) {
            throw new IllegalStateException("Channel not connected");
        }
        eof = false;
    }


    public boolean isConnected() {
        return connected;
    }


    @SuppressWarnings("unchecked")
    public <T> T attached() {
        return (T) attached;
    }

    public void attach(Object object) {
        this.attached = object;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(name)
                .add("Host", inetSocketAddress.getHostName())
                .add("Port", inetSocketAddress.getPort())
                .add("Connected", connected)
                .add("PacketListenerSize", packetsListeners.size())
                .add("ExecutorService", executorService != null).toString();
    }
}
